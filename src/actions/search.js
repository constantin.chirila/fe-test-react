// import axios from "axios";
import searchData from '../data/searchResults.json'

export const searchAction = (searchTerm) => dispatch => {
  //This should be done with axios, but I am having CORS issues
  const axiosData = {
    searchData,
    searchTerm
  }
  //This dispatch would've' been part of the successful promise from axios
  dispatch({
    type: 'SEARCH_ACTION',
    payload: axiosData
   })
 }
