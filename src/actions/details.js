// import axios from "axios"
import propertyDetails from '../data/propertyDetails.json'

 export const detailsAction = (id) => dispatch => {
  //This should be done with axios, but I am having CORS issues
  let axiosData = propertyDetails[0]
  propertyDetails.forEach(property => {
    if(property.listingId === id) {
      axiosData = property
    }
  })
  //This dispatch would've' been part of the successful promise from axios
  dispatch({
    type: 'DETAILS_ACTION',
    payload: axiosData
   })
 }
