export default (state = {}, action) => {
  switch (action.type) {
    case 'DETAILS_ACTION':
    return {
      details: action.payload
    }
   default:
    return state
  }
 }