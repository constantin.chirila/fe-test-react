import { combineReducers } from 'redux'
import searchReducer from './searchReducer'
import detailsReducer from './detailsReducer'

export default combineReducers({
 searchReducer,
 detailsReducer
});
