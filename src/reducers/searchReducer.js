export default (state = {}, action) => {
  switch (action.type) {
    case 'SEARCH_ACTION':
    return {
      metadata: action.payload.searchData.metadata,
      results: action.payload.searchData.results,
      searchTerm: action.payload.searchTerm
    }
   default:
    return state
  }
 }