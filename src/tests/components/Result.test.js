import React from 'react'
import { mount } from 'enzyme'
import Result from '../../components/Result'
import { MemoryRouter } from 'react-router-dom'

const data = {
  listingId: '11',
  images: [{
    small: 'image link',
    title: 'image'
  }],
  listPrice: '1',
  streetNumber: '2',
  streetName: 'abc',
  city: 'def',
  county: 'ghj',
  state: 'klm',
  postcode: 'ab123',
  datePosted: '2019-02-12T19:16:11.239Z',
}

it('renders without crashing', () => {
  mount(
    <MemoryRouter>
      <Result data={data} />
    </MemoryRouter>
  )
})
