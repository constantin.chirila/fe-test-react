import React from 'react'
import { shallow } from 'enzyme'
import Header from '../../components/Header'
import Search from '../../components/Search'


it('renders without crashing', () => {
  shallow(<Header />)
})

it('has a Search Component', () => {
  const wrapper = shallow(<Header />)
  expect(wrapper.find(Search).exists()).toEqual(true)
})
