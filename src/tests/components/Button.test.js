import React from 'react'
import { shallow } from 'enzyme'
import Button from '../../components/Button'

it('renders without crashing', () => {
  const handle = jest.fn()
  shallow(<Button handleClick={handle}>Test</Button>)
})

it('tests click event', () => {
  const mockCallBack = jest.fn()
  const button = shallow((<Button handleClick={mockCallBack}>Ok!</Button>))
  button.find('button').simulate('click')
  expect(mockCallBack.mock.calls.length).toEqual(1)
});