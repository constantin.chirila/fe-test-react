import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Header from './../components/Header'
import SearchResults from './SearchResults'
import ResultDetails from '../components/ResultDetails'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <main>
          <Route exact path="/search/:id" component={SearchResults} />
          <Route exact path="/property/:id" component={ResultDetails} />
        </main>
      </div>
    )
  }
}

export default App
