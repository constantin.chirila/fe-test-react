import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Result from '../components/Result'

class SearchResults extends Component {
  createList = () => {
    return this.props.searchReducer.results.map( (result, index) => {
      return (<Result data={result} key={index}/>)
    })
  }

  render() {
    return (
      <div>
        <br/>
        <h2>Results</h2>
        <div className="searchResults">
          {this.props.searchReducer.results
            ? this.createList()
            : 'No results'}
        </div>
      </div>
    )
  }
}

SearchResults.propTypes = {
  searchReducer: PropTypes.object.isRequired
}


const mapStateToProps = state => ({
  ...state
 })

export default connect(mapStateToProps)(SearchResults)
