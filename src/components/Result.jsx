import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { distanceInWords } from 'date-fns'
import PropTypes from 'prop-types'

class Result extends Component {
  getDate(date) {
    return distanceInWords(date, new Date())
  }

  goToDetails(){
    this.props.history.push('/property/' + this.props.data.listingId)
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.goToDetails()
    }
  }

  render() {
    return (
      <div
        className=
        "result" tabIndex="0"
        onClick={this.goToDetails.bind(this)}
        onKeyPress={this.handleKeyPress.bind(this)}
      >
        <img src={this.props.data.images[0].small} alt={this.props.data.images[0].title}/>
        <div className="content">
          <h2>${this.props.data.listPrice}</h2>
          <div className="address">
            {this.props.data.streetNumber},
            {this.props.data.streetName},
            {this.props.data.city},
            {this.props.data.county},
            {this.props.data.state} {this.props.data.postcode}
          </div>
          Posted on PurpleBricks {this.getDate(this.props.data.datePosted)} ago
        </div>
      </div>
    )
  }
}

Result.propTypes = {
  data: PropTypes.object.isRequired
}

export default withRouter(Result)
