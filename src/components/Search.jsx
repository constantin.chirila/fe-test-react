import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Button from './Button'
import { searchAction } from '../actions/search'

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchValue: '',
      errorMsg: ''
    }
  }

  searchAction() {
    if(this.state.searchValue) {
      this.props.searchAction(this.state.searchValue)
      this.props.history.push('/search/' + this.state.searchValue)
    } else {
      this.setState({errorMsg: 'Search field is empty. Enter a city name.'})
    }
  }

  handleChange(e) {
    this.setState({ searchValue: e.target.value })
    this.setState({errorMsg: ''})
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.searchAction()
    }
  }

  render() {
    return (
      <div className="search">
        <h1>Purple Bricks Property Search</h1>
        <div className="searchContainer">
          <input
            type="text"
            placeholder="City name"
            value={this.state.searchValue}
            onChange={ this.handleChange.bind(this) }
            onKeyPress={this.handleKeyPress.bind(this) }
            />
          <div className="errorMsg">
            {this.state.errorMsg}
          </div>
          <Button handleClick={this.searchAction.bind(this)}>
            Search Properties
          </Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
searchAction: (searchTerm) => dispatch(searchAction(searchTerm))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search))
