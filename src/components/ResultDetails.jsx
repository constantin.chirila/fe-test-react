import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { detailsAction } from '../actions/details'
import Button from './Button'
import PropTypes from 'prop-types'

class ResultDetails extends Component {
  constructor(props){
    super(props)
    this.state = {
      search: this.props.searchReducer.searchTerm ? 'Go back to results' : 'Go back to search'
    }
  }

  componentDidMount() {
    this.props.detailsAction(this.props.match.params.id)
  }

  generateThumbs(thumbs) {
    return thumbs.map((thumb, index) => (
      <div className={'thumb ' + this.isThumbSelected(index, 0)}  key={index}>
        <img src={thumb.small} alt={thumb.title}/>
      </div>
    )).splice(0, 4)
  }

  isThumbSelected(index, selected) {
    return index === selected ? 'selected' : ''
  }

  goBack(){
    if(this.props.searchReducer.searchTerm) {
      this.props.history.push('/search/' + this.props.searchReducer.searchTerm)
    } else {
      this.props.history.push('/')
    }
  }

  render() {
    const details = this.props.detailsReducer.details
    if(details) {
      return (
        <div className="searchResultDetails clear">
          <div className="clear">
            <Button handleClick={this.goBack.bind(this)}>
              {this.state.search}
            </Button>
          </div>
          <div className="gallery">
            <img src={details.images[0].medium} alt={details.images[0].medium}/>
            { this.generateThumbs(details.images) }
          </div>
          <div className="details">
            <h1>${details.listPrice}</h1>
            <h3>{details.streetNumber}, {details.streetName}, {details.city}, {details.county}, {details.state} {details.postcode}</h3>
            <p>{details.description}</p>
          </div>
        </div>
      )
    }
    return (<div></div>)
  }
}


ResultDetails.propTypes = {
  searchReducer: PropTypes.object.isRequired,
  detailsReducer: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  ...state
})

const mapDispatchToProps = dispatch => ({
  detailsAction: (id) => dispatch(detailsAction(id))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResultDetails))
