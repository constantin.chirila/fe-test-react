import React from 'react'
import PropTypes from 'prop-types'

const Button = (props) => {
  return(
    <button onClick={props.handleClick.bind(this)} className="button">
      { props.children }
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired
}
export default Button
