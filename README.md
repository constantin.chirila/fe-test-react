This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits and you will see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Project Choices

1. **BUILD** - I have chosen to use Create-React-App mainly due to the speed of setting up a project. As it had everything I needed I haven't had a use case where I would need to change the Webpack configuration.
2. **API** - I have seemed to have some serious issues with CORS when calling the provided API's. Even installing a Chrome plugin to disable the inbuilt CORS didn;'t quite work as I would get an empty data object for some reason. Using Postman I've managed to create some dummy data to use in my application. To view how I would the api in a react-redux application you can also look at a clone of this project: [FE Test with live API](https://gitlab.com/constantin.chirila/fe-test-touch.git)
3. **SCAFFOLDING** - I have decided to scaffold the project in this manner (opposite to having a folder for each component) because I thought to be fit for a project of this size without css-in-js and other complicated component systems. I've also classified components into _Components_ and _Containers_.
4. Decided to use **date-fns** as a performance consideration. The famous MomentJS can be quite bulky. Especially as it come with all locales by default.

## Given more time I would've done the following

1. Go more in detail with testing.
2. Add typescript to the project.
3. ~~Add~~ improve prop-types of my components.
4. Study best practices with React to write better code.  Coming from a VueJS environment, makes me approach React with a different mindset, which might not be as efficient for the React ecosystem.
5. A thorough audit of accessibility.
6. Think of a better way of scaffolding components. I am not 100% satisfied with the current component system. I am used having the tests separately but it might benefit from having it in the same folder.
7. Look deeper into react performance considerations and how I can reduce the bundle size. Maybe even setup tree shaking (especially for a bigger application).
8. Added functionality to the property gallery.
